<?php

namespace RhubarbBlog\Leaves\AboutUs;

class AboutUsView extends \Rhubarb\Leaf\Views\View
{
    protected function printViewContent()
    {
        ?>
        <h1>About Us</h1>
        <p>This is a blog site created using RhubarbPHP.</p>
        <?php
    }
}