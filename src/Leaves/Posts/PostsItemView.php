<?php

namespace RhubarbBlog\Leaves\Posts;

use Rhubarb\Leaf\Crud\Leaves\CrudView;

class PostsItemView extends CrudView
{
    protected function createSubLeaves()
    {
        parent::createSubLeaves();
        $this->registerSubLeaf(
            "Title",
            "Content"
        );
    }

    protected function printViewContent()
    {
        print $this->leaves["Title"];
        print "<br>";
        print $this->leaves["Content"];
        print "<br>";
        print $this->leaves["Save"];
        print $this->leaves["Cancel"];
        print $this->leaves["Delete"];
    }
}