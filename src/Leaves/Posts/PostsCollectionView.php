<?php

namespace RhubarbBlog\Leaves\Posts;

use Rhubarb\Leaf\Crud\Leaves\CrudView;
use RhubarbBlog\Models\Post;

class PostsCollectionView extends CrudView
{
    protected function createSubLeaves()
    {
        parent::createSubLeaves();
        $this->registerSubLeaf(
            $table = new \Rhubarb\Leaf\Table\Leaves\Table(Post::all(), 50, "PostsTable")
        );

        $table->columns =
            [
                "Title",
                "Edit" => '<a href="/posts/{PostID}/">edit</a>'
            ];
    }

    protected function printViewContent()
    {
        parent::printViewContent();

        print '<a href="/posts/add/">add</a><br>';
        print $this->leaves["PostsTable"];
    }
}