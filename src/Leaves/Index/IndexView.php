<?php

namespace RhubarbBlog\Leaves\Index;

use Rhubarb\Leaf\Views\View;

class IndexView extends \Rhubarb\Leaf\Crud\Leaves\CrudView
{
    protected function printViewContent()
    {
        parent::printViewContent();
        ?>
        <h1>Rhubarb Blog</h1>
        <?php

        $posts = \RhubarbBlog\Models\Post::all();
        foreach ($posts as $post) {
            ?>
            <h2><?= $post->Title ?></h2>
            <p><?= $post->Content ?></p>
            <?php
        }
    }
}