<?php

namespace RhubarbBlog;

use Rhubarb\Crown\Application;
use Rhubarb\Crown\Layout\LayoutModule;
use Rhubarb\Crown\UrlHandlers\ClassMappedUrlHandler;
use Rhubarb\Leaf\LeafModule;
use Rhubarb\Stem\Repositories\MySql\MySql;
use Rhubarb\Stem\Repositories\Repository;
use Rhubarb\Stem\Schema\SolutionSchema;
use Rhubarb\Stem\StemModule;
use RhubarbBlog\Layouts\DefaultLayout;
use RhubarbBlog\Leaves\AboutUs\AboutUs;
use RhubarbBlog\Leaves\Index\Index;
use RhubarbBlog\Models\RhubarbBlogSolutionSchema;

class RhubarbBlog extends Application
{
    protected function initialise()
    {
        parent::initialise();

        if (file_exists(APPLICATION_ROOT_DIR . "/settings/site.config.php")) {
            include_once(APPLICATION_ROOT_DIR . "/settings/site.config.php");
        }

        SolutionSchema::registerSchema("RhubarbBlogSolutionSchema", RhubarbBlogSolutionSchema::class);
        Repository::setDefaultRepositoryClassName(MySql::class);
    }

    protected function registerUrlHandlers()
    {
        parent::registerUrlHandlers();
        $this->addUrlHandlers(
            [
                "/" => new ClassMappedUrlHandler(Index::class,
                    [
                        "about-us/" => new ClassMappedUrlHandler(AboutUs::class),
                        "posts/" => new \Rhubarb\Leaf\Crud\UrlHandlers\CrudUrlHandler(\RhubarbBlog\Models\Post::class, \Rhubarb\Crown\String\StringTools::getNamespaceFromClass(\RhubarbBlog\Leaves\Posts\PostsCollectionView::class))
                    ])
            ]
        );
    }

    protected function getModules()
    {
        return [
            new LayoutModule(DefaultLayout::class),
            new LeafModule(),
            new StemModule()
        ];
    }
}