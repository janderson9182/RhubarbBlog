<?php

namespace RhubarbBlog\Models;

use Rhubarb\Stem\Models\Model;

/**
 *
 *
 * @property int $PostID Repository field
 * @property string $Title Repository field
 * @property string $Content Repository field
 */
class Post extends Model
{
    protected function createSchema()
    {
        $schema = new \Rhubarb\Stem\Schema\ModelSchema("Post");

        $schema->addColumn(
            new \Rhubarb\Stem\Schema\Columns\AutoIncrementColumn("PostID"),
            new \Rhubarb\Stem\Schema\Columns\StringColumn("Title", 50),
            new \Rhubarb\Stem\Schema\Columns\LongStringColumn("Content")
        );

        $schema->labelColumnName = "Title";

        return $schema;
    }
}